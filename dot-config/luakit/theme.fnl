(local menu_bg "#282c34")
(local menu_fg "#abb2bf")
(local hint_overlay_border "1px dotted #000")

; Default settings
{ :font "13px Source Sans Pro"
  :fg   "#f00"
  :bg   "#000"

; Genaral colours
  :success_fg "#0f0"
  :loaded_fg  "#33AADD"
  :error_fg   "#FFF"
  :error_bg   "#F00"

; Warning colours
  :warning_fg "#F00"
  :warning_bg "#FFF"

; Notification colours
  :notif_fg "#444"
  :notif_bg "#FFF"

; Menu colours
  :menu_fg                 menu_fg
  :menu_bg                 menu_bg
  :menu_selected_fg        "#000"
  :menu_selected_bg        "#FF0"
  :menu_title_bg           "#fff"
  :menu_primary_title_fg   "#f00"
  :menu_secondary_title_fg "#666"

  :menu_disabled_fg "#999"
  :menu_disabled_bg menu_bg
  :menu_enabled_fg  menu_fg
  :menu_enabled_bg  menu_bg
  :menu_active_fg   "#060"
  :menu_active_bg   menu_bg

; Proxy manager
  :proxy_active_menu_fg   "#000"
  :proxy_active_menu_bg   "#FFF"
  :proxy_inactive_menu_fg "#888"
  :proxy_inactive_menu_bg "#FFF"

; Statusbar specific
  :sbar_fg         "#abb2bf"
  :sbar_bg         "#222"

; Downloadbar specific
  :dbar_fg       "#abb2bf"
  :dbar_bg       "#282c34"
  :dbar_error_fg "#e06c75"

; Input bar specific
  :ibar_fg "#abb2bf"
  :ibar_bg "rgba(0,0,0,0)" ; the carret is white, dark BG makes it visible

; Tab label
  :tab_fg          "#888"
  :tab_bg          "#222"
  :tab_hover_bg    "#292929"
  :tab_ntheme      "#ddd"
  :selected_fg     "#fff"
  :selected_bg     "#000"
  :selected_ntheme "#ddd"
  :loading_fg      "#3AD"
  :loading_bg      "#000"

  :selected_private_tab_bg "#3d295b"
  :private_tab_bg          "#22254a"

; Trusted/untrusted ssl colours
  :trust_fg   "#98c379"
  :notrust_fg "#be5046"

; Follow mode hints
  :hint_font       "8px sans-serif"
  :hint_fg         "#fff"
  :hint_bg         "#357a"
  :hint_border     "1px #000"
  :hint_overlay_bg "rgba(255,255,153,0.3)"
  :hint_overlay_border          hint_overlay_border
  :hint_overlay_selected_bg     "rgba(0,255,0,0.3)"
  :hint_overlay_selected_border hint_overlay_border

; General colour pairings
  :ok    { :fg "#abb2bf" :bg "#222" }
  :warn  { :fg "#282c34" :bg "#d19a66" }
  :error { :fg "#282c34" :bg "#be5046" }
}
