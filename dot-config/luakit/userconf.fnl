; # Settings clarifications
;
; Two types of settings, user API and module API
; see https://github.com/luakit/luakit/issues/660
;
; The following uses User API, and will not be persisted! (aka: available
; after restart):
;
;     (tset (. settings :on host :webview) :zoom_level win.view.zoom_level)
;
; With the following, the change should be persisted:
;
;     (settings.set_setting
;       "webview.zoom_level"
;       (* win.view.zoom_level 100)
;       { :domain host })
;
; TODO:
; * Create a Ublock-origin by using the page class, trivially it would need to
;   spawn the process with `ipc.require_web_module`?
; * git check-in stylesheets
; * replace newtab with todo-list + frequent links

; Enable some plugins
(require :readline)
(local log (require :msg))
(local modes (require :modes))
(local settings (require :settings))
(local soup (require :soup))
(local webview (require :webview))

; Setting select keys
; ======= ====== ====
;
; Use upercase for label selection, and use lowercase for text-based
; selection
(local select (require :select))
(local follow (require :follow))
(set select.label_maker #(->> "ASDFHJKLG" charset reverse sort trim))
(set follow.pattern_maker
  #(if (string.match $1 "^[A-Z]*$")
    (follow.pattern_styles.match_label $1)
    (follow.pattern_styles.re_match_text $1)))


; Zooming
; =======
(fn js [win js-body callback]
  "Evaluate `js-body` within the body of a function and call `callback`
  (a lua function) with the result"
  (win.view:eval_js (.. "(function(){" js-body "})()") { : callback }))

(fn anchored-zoom [direction win]
  "Keep scroll location when zooming, and save zoom levels for domains"
  (js win
    "let scrollHeight = window.document.documentElement.scrollHeight;
     let windowHeight = Math.max(scrollHeight - window.innerHeight, 0);
     return window.scrollY * 100 / windowHeight;"
    (fn [pct-scroll]
      ((. win direction) win (settings.get_setting "window.zoom_step"))
      (local { : host } (soup.parse_uri win.view.uri))
      (local pct-zoom (* win.view.zoom_level 100))
      (settings.set_setting "webview.zoom_level" pct-zoom { :domain host })
      (win:scroll { :ypct pct-scroll }))))

; remapfest
; =========
(fn add-cmd [mode cmd-key description to-execute]
  (modes.add_binds mode [[cmd-key description to-execute]]))

(fn add-zoom-map [shortcut direction]
  "Wrapper to add key bindings for `anchored-zoom`"
  (local description (.. "zoom " direction " (keeps scroll location)"))
  (local command (.. :zoom_ direction))
  (add-cmd :normal shortcut description (partial anchored-zoom command)))

(add-zoom-map :e :in)
(add-zoom-map :E :out)

(fn chrome-open [shortcut doc-string path]
  "Open any `luakit://` chrome page"
  (add-cmd :normal shortcut doc-string #($:navigate (.. "luakit://" path))))

(chrome-open :a "Open API reference" "help/doc/index.html")
(chrome-open :b "Open bookmarks" "bookmarks")
(chrome-open :h "Open history" "history")
(chrome-open :l "Open logs" "log")

(fn remap [mappings]
  "batch remapping of pre-existing bindings"
  (each [mode to-change (pairs mappings)]
    (var remap-arg [])
    (each [from to (pairs to-change)]
      (table.insert remap-arg [to from])
      (modes.remove_binds mode [to]))
    (modes.remap_binds mode remap-arg)))

(remap
  { :ex-follow { ";" "." }
    :normal    { ";" "." ":" "-" }
    :all       { "<Control-[>" "<Control-l>" }
  })


(fn webview-init-signal [signal callback]
  "Add a signal handler"
  (webview.add_signal :init (fn [view] (view:add_signal signal callback))))

; www.reddit.com -> old.reddit.com
(webview-init-signal "navigation-request"
  (fn [v uri]
    (local parsed (soup.parse_uri uri))
    (when (= parsed.host "www.reddit.com")
      (set parsed.host "old.reddit.com")
      (set v.uri (soup.uri_tostring parsed))
      false)))

{}
