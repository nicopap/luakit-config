# Luakit config

This is part of my public dotfiles repositories. I discovered fennel, then
luakit, and decided to have a completely hackable web browser!

The config is written in [fennel](https://fennel-lang.org/) and you will need
it to generate the lua files. The `Makefile` is here to simplify the process.
Currently, luakit doesn't support hot reloading, so you'll have to write your
config offline. (that means you have to use the `:restart` command in luakit)


## Deployment

I use [`stow`](https://www.gnu.org/software/stow/) for dotfile management.

Once `stow` and `git` are installed on a computer, and that computer has
internet access, I just run:

```sh
git clone https://gitlab.com/nicopap/luakit-config.git

stow --dotfiles --ignore='README\.md|\.git.*' -t ~ -v3 luakit-config
```

You'll need to install the adblock lists in the data dir (on xdg-compliant
systems, it is `~/.local/share/luakit`.


## Subtreeing

I have a (private) general dotfiles repository that I use when setting up a new
machine. This repo is simply `subtree`-ed (see `man git-subtree`) in the
general repo.
